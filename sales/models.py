from django.db import models

CURRENCY = [
    ('USD', '$'),
    ('KGS', 'сом')
]

TAG = [
    ('exchange', 'Обмен'),
    ('free', 'Даром')
]


# CATEGORY2 = [
#     ('Опрыскиватели навесные', 'Опрыскиватели навесные'),
#     ('Опрыскиватели прицепные', 'Опрыскиватели прицепные'),
#     ('Плуги', 'Плуги'),
#     ('Компакторы, бороны', 'Компакторы, бороны'),
#     ('Культиваторы', 'Культиваторы'),
#     ('Сеялки точного высева', 'Сеялки точного высева'),
#     ('Сеялки зерновые', 'Сеялки зерновые'),
#     ('Сеялки прочие', 'Сеялки прочие'),
# ]


class Category(models.Model):
    name = models.CharField('Название категорий', max_length=128, blank=True, null=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField('Название Подкатегорий навесное и прицепное', max_length=128, blank=True, null=True)

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'

    def __str__(self):
        return self.name


class ProductCategory(models.Model):
    class Meta:
        verbose_name = 'Товар Подкатегории'
        verbose_name_plural = 'Товары Подкатегории'
        ordering = ['id']

    price = models.PositiveIntegerField('Цена товара', blank=True, null=True, default="Договорная")
    currency = models.CharField('Валюта', choices=CURRENCY, max_length=16, blank=True, null=True)
    title = models.CharField('Заголовок товара', max_length=128, blank=True, null=True)
    category = models.ManyToManyField(SubCategory, verbose_name='Категория товара', blank=True,
                                      null=True)
    phone_number = models.CharField('Номер телефона', max_length=9, blank=True, null=True)
    date = models.DateField('Дата объявления', max_length=18, auto_now=True, blank=True, null=True)
    tag = models.CharField('Тег продукта', choices=TAG, max_length=16, blank=True, null=True)
    region = models.CharField('Область и Регион', max_length=128, blank=True, null=True)
    city = models.CharField('Город и Район', max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.title)


# on_delete=models.SET_NULL,
class Product(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['id']

    price = models.PositiveIntegerField('Цена товара', blank=True, null=True, default=0)
    currency = models.CharField('Валюта', choices=CURRENCY, max_length=16, blank=True, null=True)
    title = models.CharField('Заголовок товара', max_length=128, blank=True, null=True)
    category = models.ManyToManyField(Category, verbose_name='Категория товара', blank=True,
                                      null=True)
    phone_number = models.CharField('Номер телефона', max_length=9, blank=True, null=True)
    date = models.DateField('Дата объявления', max_length=18, auto_now=True, blank=True, null=True)
    tag = models.CharField('Тег продукта', choices=TAG, max_length=16, blank=True, null=True)
    region = models.CharField('Область и Регион', max_length=128, blank=True, null=True)
    city = models.CharField('Город и Район', max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.title)


class ProductImage(models.Model):
    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    image = models.ImageField('Фото продукта', upload_to="sales_photo/", blank=True, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True,
                                null=True)

    def __str__(self):
        return str(self.product)
