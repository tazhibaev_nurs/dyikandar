from django.urls import path
from .views import ProductView, ProductImageView, ProductCategoryView, CategoryView, SubCategoryView

urlpatterns = [
    path('product/', ProductView.as_view({'get': 'list', 'post': 'create'})),
    path('product/<int:pk>/', ProductView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('product/category/', CategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('product/category/<int:pk>/', CategoryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('image/', ProductImageView.as_view({'get': 'list', 'post': 'create'})),
    path('image/<int:pk>/', ProductImageView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('product2/', ProductCategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('product2/<int:pk>/', ProductCategoryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path('product2/subcategory/', SubCategoryView.as_view({'get': 'list', 'post': 'create'})),
    path('product2/subcategory/<int:pk>/', SubCategoryView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),

]
