from collections import OrderedDict
from django.shortcuts import render
from rest_framework import filters, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .models import Product, ProductImage, ProductCategory, Category, SubCategory
from .serializers import ProductSerializer, ProductImageSerializer, ProductCategorySerializer, CategorySerializer, \
    SubCategorySerializer


class Pagination(PageNumberPagination):
    page_size = 10
    page_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class SubCategoryView(ModelViewSet):
    model = SubCategory
    serializer_class = SubCategorySerializer
    queryset = SubCategory.objects.all()
    lookup_field = 'pk'


class CategoryView(ModelViewSet):
    model = Category
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'pk'


class ProductImageView(ModelViewSet):
    model = ProductImage
    serializer_class = ProductImageSerializer
    queryset = ProductImage.objects.all()
    lookup_field = 'pk'


class ProductCategoryView(ModelViewSet):
    model = ProductCategory
    serializer_class = ProductCategorySerializer
    queryset = ProductCategory.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination
    filter_backends = [filters.SearchFilter]
    search_fields = '__all__'

    def create(self, request, *args, **kwargs):
        serializer = ProductCategorySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductView(ModelViewSet):
    model = Product
    serializer_class = ProductSerializer
    lookup_field = 'pk'
    pagination_class = Pagination
    filter_backends = [filters.SearchFilter]
    search_fields = '__all__'

    def get_queryset(self):
        queryset = Product.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('image'):
            filter_fields['image'] = self.request.GET.get('image')
        if self.request.GET.get('title'):
            filter_fields['title'] = self.request.GET.get('title')
        if self.request.GET.get('price'):
            filter_fields['price'] = self.request.GET.get('price')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
